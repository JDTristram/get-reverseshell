# Example file for upstream/metadata.
# See https://wiki.debian.org/UpstreamMetadata for more info/fields.
# Below an example based on a github project.

# Bug-Database: https://github.com/<user>/get-reverseshell/issues
# Bug-Submit: https://github.com/<user>/get-reverseshell/issues/new
# Changelog: https://github.com/<user>/get-reverseshell/blob/master/CHANGES
# Documentation: https://github.com/<user>/get-reverseshell/wiki
# Repository-Browse: https://github.com/<user>/get-reverseshell
# Repository: https://github.com/<user>/get-reverseshell.git
